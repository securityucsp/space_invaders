/* Server code in C */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <iostream>
#include <thread>
#include <vector>

using namespace std;

vector<int> clients;

void Broadcast(char* message, int size) {

  for (int i = 0; i < clients.size(); ++i) {
    write(clients[i], message, size);
  }

}

void GetAction(char* message, int size) {
  Broadcast(message, size);
}


void ListenClient(int ConnectFD) {

  char buffer[256];
  int n;
  
  do
  {
    buffer[0] = 0;
    n = read(ConnectFD, buffer, 255);
    buffer[n] = 0;
    cout<<"Received: "<<buffer<<endl;
    GetAction(buffer, n);

  } while (*buffer != '#');
  
}

void GetConnection(int &SocketFD, socklen_t &size){

  struct sockaddr_in stSockAddr;
  struct sockaddr_in cli_addr;

  if ((SocketFD = socket(AF_INET, SOCK_STREAM, 0)) == -1)
  {
    perror("Socket");
    exit(1);
  }

  if (setsockopt(SocketFD, SOL_SOCKET, SO_REUSEADDR, "1", sizeof(int)) == -1)
  {
    perror("Setsockopt");
    exit(1);
  }

  stSockAddr.sin_family = AF_INET;
  stSockAddr.sin_port = htons(3000);
  stSockAddr.sin_addr.s_addr = INADDR_ANY;

  if (bind(SocketFD, (struct sockaddr *)&stSockAddr, sizeof(struct sockaddr)) == -1)
  {
    perror("Unable to bind");
    exit(1);
  }

  if (listen(SocketFD, 5) == -1)
  {
    perror("Listen");
    exit(1);
  }

  size = sizeof(stSockAddr);
}

int main(int argc, char** argv) {
  socklen_t size;
  int SocketFD;
  int n;

  GetConnection(SocketFD, size);

  while(1)
  {
    int ConnectFD = accept(SocketFD, NULL, NULL);
    thread(ListenClient, ConnectFD).detach();
    clients.push_back(ConnectFD);
  }

  close(SocketFD);
  cout<<"Bye  bye :D"<<endl;
  return 0;
}
