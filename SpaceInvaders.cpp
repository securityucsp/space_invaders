#include <iostream>
#include <vector>
#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

using namespace std;

struct SpaceShipStatus {
    int x;
    int y;
    bool alive;
};

class SpaceShip {

    private:
        int id;
        char label;
        SpaceShipStatus status;
    
    public:
        SpaceShip (char label) {
            this->id = label;
            this->label = label;
        }
        void setPosition (int x, int y) {
            this->status.x = x;
            this->status.y = y;
            return;
        }
        void moveRight () {
            this->status.x++;
        }
        void moveLeft () {
            this->status.x--;
        }
        void moveUp () {
            this->status.y--;
        }
        void moveDown () {
            this->status.y++;
        }
        int getId () {
            return this->id;
        }
        char getLabel () {
            return this->label;
        }
        SpaceShipStatus getStatus () {
            return this->status;
        }
        void publishStatus () { /**/ }

};

class Space {
    
    private:
        vector<SpaceShip*> spaceShips;
        int width;
        int height;

    public:
        Space (int width, int height) {
            this->width = width;
            this->height = height;
        }
        void registerSpaceShip (SpaceShip* spaceShip) {
            this->spaceShips.push_back(spaceShip);
        }
        void renderSpace () {
            system("clear");
            for (int y=0; y<this->height; y++) {
                for (int x=0; x<this->width; x++) {
                    // char buffChar = (rand() % 2) ? '*' : ' ';
                    char buffChar = '_';
                    // Iterate spaceships
                    for (int i=0; i<this->spaceShips.size(); i++) {
                        if (
                            (
                                this->spaceShips[i]->getStatus().x == x ||
                                this->spaceShips[i]->getStatus().x == x+1
                            ) && (
                                this->spaceShips[i]->getStatus().y == y ||
                                this->spaceShips[i]->getStatus().y == y+1
                            )
                        ) {
                            buffChar = this->spaceShips[i]->getLabel();
                        }
                    }
                    cout << buffChar;
                }
                cout << endl;
            }
        }
};

/* int main () {

    // Clean random
    srand (time(NULL));

    // Init space
    Space space(100, 50);
    
    // Create demo spaceship
    SpaceShip happySpaceShip('A');
    happySpaceShip.setPosition(50, 30);
    space.registerSpaceShip(&happySpaceShip);

    // Render space
    space.renderSpace();

    while (1) {
        char pos;
        pos = getchar();
        switch (pos) {
            case 'w':
                happySpaceShip.moveUp();
                space.renderSpace();
            break;
            case 'd':
                happySpaceShip.moveRight();
                space.renderSpace();
            break;
            case 'a':
                happySpaceShip.moveLeft();
                space.renderSpace();
            break;
            case 's':
                happySpaceShip.moveDown();
                space.renderSpace();
            break;
        }
    }    

    return 0;
} */