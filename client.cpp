/* Client code in C */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <iostream>
#include <vector>
#include <time.h> /* time */
#include <thread>

#include "./SpaceInvaders.cpp"

using namespace std;

void send(int SocketFD, char act, char id, char x, char y, char label)
{

  int n;
  string message, foo;

  message[0] = act;
  message[1] = id;
  message[2] = x;
  message[3] = y;
  message[4] = label;

  n = write(SocketFD, message.c_str(), 5);
}

void Listen(int SocketFD)
{
  char read_buffer[256];
  int n;

  do
  {
    n = read(SocketFD, read_buffer, 256);
    cout << "Server says: " << read_buffer << endl;
  } while (read_buffer[0] != '#');
}

int main(void)
{

  // Clean random
  srand(time(NULL));
  struct sockaddr_in stSockAddr;
  int Res;
  int SocketFD = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);

  int n;

  if (-1 == SocketFD)
  {
    perror("cannot create socket");
    exit(EXIT_FAILURE);
  }

  memset(&stSockAddr, 0, sizeof(struct sockaddr_in));

  stSockAddr.sin_family = AF_INET;
  stSockAddr.sin_port = htons(3000);

  Res = inet_pton(AF_INET, "192.168.110.130", &stSockAddr.sin_addr);

  if (0 > Res)
  {
    perror("error: first parameter is not a valid address family");
    close(SocketFD);
    exit(EXIT_FAILURE);
  }
  else if (0 == Res)
  {
    perror("char string (second parameter does not contain valid ipaddress");
    close(SocketFD);
    exit(EXIT_FAILURE);
  }

  if (-1 == connect(SocketFD, (const struct sockaddr *)&stSockAddr, sizeof(struct sockaddr_in)))
  {
    perror("connect failed");
    close(SocketFD);
    exit(EXIT_FAILURE);
  }

  thread(Listen, SocketFD).detach();

  // Space
  /* Space space(100, 50);

  // Create happy spaceship
  char symbol;
  cout<<"Type your label: "<<endl;
  cin>>symbol;
  SpaceShip happySpaceShip(symbol);
  happySpaceShip.setPosition(50, 30);
  send(SocketFD, 'r', happySpaceShip.getId(), happySpaceShip.getStatus().x, happySpaceShip.getStatus().y, happySpaceShip.getLabel());
  space.registerSpaceShip(&happySpaceShip); */

  string foo;
   send(SocketFD,'a','a','a','a','a');
   while(foo!="#"){
     send(SocketFD,'a','a','a','a','a');
     cin>>foo;
   }

  return 0;
}